//Import the module express.
var express =  require('express');
//Instance the object route the express.
var router = express.Router();
// Import the module model bicicletas.
var bicicletaController = require('../../controllers/api/bicicletasControllerApi');

// create the routes.
router.get('/',bicicletaController.bicicleta_list);
router.post('/create',bicicletaController.bicicleta_create);
router.delete('/delete',bicicletaController.bicicletas_delete);

//Export the route. 
module.exports = router;