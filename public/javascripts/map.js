var map = L.map('main_map').setView([10.1993026,-64.7221444], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);


//L.marker([10.183579,-64.692893]).addTo(map);    

//cargar mapa con las biciclestas usando la api.
// AJAX es un req asincronico, http para hacer solicitud a una web en un formato especificado.
$.ajax({
    dataType: "json",
    url:"api/bicicletas", //local
    success:function(result){
        console.log(result);
        result.bicicleta.forEach(function(bici) {
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
})