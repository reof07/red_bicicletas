//Creamos el modelo bicicleta.
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// paso 1: definimos el modelo.
// index de tipo geografico: '2dsphre'
var bicicletaSchema = new Schema({
    code: String,
    color: String,
    modelo: String,
    ubicacion: {
        type : [Number], index: {type: '2dsphre', sparse:true}
    }
});

// instancia de bicicletas
bicicletaSchema.statics.createInstance = function(code,color,modelo,ubicacion){
    return new this({
        code:code,
        color:color,
        modelo:modelo,
        ubicacion:ubicacion
    });
};

// Creamos el metodo getAll
bicicletaSchema.statics.allBicis = function(cb){
    return this.find({},cb);
}

// Creamos el metodo add.
bicicletaSchema.statics.add = function(aBici,cb){
    this.create(aBici,cb);
}

bicicletaSchema.statics.findByCode = function(aCode,cb){
    return this.findOne({code:aCode}, cb);
};

bicicletaSchema.statics.removeByCode = function(aCode,cb){
    return this.deleteOne({code:aCode},cb);
};

// para crear metodos de instnacias.
bicicletaSchema.methods.toString = function(){
    return 'code: ' + this.code + '| color: ' + this.color;
}

// paso 2: exportamos el modelo.
//const bicicleta = mongoose.model('bicicleta',bicicletaSchema); 
module.exports = mongoose.model('Bicicleta',bicicletaSchema);