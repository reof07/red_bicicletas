//var Bicicleta = require('../../models/bicicleta');
var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas',function(){
    //Creamos un before para conectar la base de datos (testDB)
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb'; 
        mongoose.connect(mongoDB, {useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true });
        const db = mongoose.connection;

        db.on('error', console.error.bind(console, 'connection error:'));
        db.once('open', function() {
            console.log('we are connected to test database!');
            done();
        });
    });

    //Creamos un after para desconectar y borrar toda la base de datos
    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err,success){
            if (err) console.log(err);
            done();
        });
    });
    
    //Test para crear instances.
    describe('Bicicleta.createInstance', ()=>{
        it('Creamos una instancia de biciletas', ()=>{
            var bici = Bicicleta.createInstance(1,'verde','urbana',[-34.5,-54.1]);
            console.log(bici);  
            expect(bici.code).toBe('1');
            expect(bici.color).toBe('verde');
            expect(bici.modelo).toBe('urbana');
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-54.1);
        });
    });

    describe('Bicicletas.allBicis',()=>{
        it('Comienza vacia',(done)=>{
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicletas.add',()=>{
        it('Agregar solo una bici',(done)=>{
            var abici =  new Bicicleta({code:'1',color:"verde",modelo:"urbana"});
            Bicicleta.add(abici,function(err,newBici){
                if(err) console.log(err);{
                    Bicicleta.allBicis(function(err,bicis){
                        expect(bicis.length).toBe(1);
                        expect(bicis[0].code).toEqual(abici.code);
                        done();
                    });
                };
            });
        });
    });

    describe('Bicicleta.findByCode',()=>{
        it('Debe devolver una bici con code 1',(done)=>{
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code:"1",color:"verde",modelo:"urbano"});
                Bicicleta.add(aBici,function(err,newBicis){
                    if(err) console.log(err);

                    var aBici2 = new Bicicleta({code:"2",color:"verde",modelo:"urbano"});
                    Bicicleta.add(aBici2,function(err,newBicis){
                        if(err) console.log(err);
                        Bicicleta.findByCode("1",function(err,targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.modelo).toBe(aBici.modelo);
                            expect(targetBici.color).toBe(aBici.color);
                            done();
                        });
                    });
                });
            });
        });
    });

    describe('Bicicleta.removeById',()=>{
        it('Borrar por id',(done)=>{
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code:"1",color:"verde",modelo:"urbano"});
                Bicicleta.add(aBici,function(err,newBici){
                    if(err) console.log(err);

                    var aBici2 = new Bicicleta({code:"2",color:"verde",modelo:"urbano"});
                    Bicicleta.add(aBici2,function(err,bici){
                        if(err) console.log(err);
                        Bicicleta.removeByCode("1");
                        expect(Bicicleta.allBicis.length).toBe(1);
                        done();
                    });
                });
            });
        });
    });  
});
