var Bicicleta = require('../../models/bicicleta');
var request = require('request'); // para el manejo de solicitudes http.
var server = require('../../bin/www'); // Exporta la variale del server.


var url_basic = 'http://localhost:5000/api/bicicletas';

/**
 * Test a las solicirudes http de la API.
 */

describe('Bicicleta API',()=>{

    describe('GET BICICLETAS /',()=>{
        it('Estatus 200', ()=>{
            expect(Bicicleta.allBicis.length).toBe(0);
            var a= new Bicicleta(1,'rojo','urbana',[10.183894, -64.689322]);
            Bicicleta.add(a);

            // Para utilizar un GET request 
            request.get(url_basic,function(error,response,body){
                expect(response.statusCode).toBe(200);
            });
        });
    });
    
    describe('POST BICICLETA /',()=>{
        it('Estatus 200',(done)=>{
            var headers = {'content-type':'application/json'};
            var aBici = '{"id":10,"color":"rojo","modelo":"urbana","lat":-64,"long":34}';
            
            request.post({
                headers: headers,
                url:url_basic,
                body: aBici
            },function(error,response,body){
                    expect(response.statusCode).toBe(200);
                    expect(Bicicleta.findById(10).color).toBe('rojo');
                    done();// done termina el proceso.
                });
            });
        });
});