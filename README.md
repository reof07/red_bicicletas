# Como crear el esqueleto de nuestra aplicacion con express generator.

## Instalamos de forma global: para esto nos dirigimos a nuestra terminal.

- **npm install -g express-generator**.
- **express -help** observamos las opciones disponibles para crear nuestro proyecto.
- Una vez instalado pasamos a crear nuestro proyecto. **(express red-biciclestas --view=pug)** 

**NOTA** las view engine permite crear documentos HTML para las vistas de nuestro proyecto. **pug**.

- **cd red-bicicletas**  
- **npm install** para installar las dependencias.
- **npm start** para probar la app.
- Luego colocamos en el buscador **http://127.0.0.1:3000/**

## Para subir los contenidos al repositorio git.

- En la carpeta del proyecto colocamos **git init*
- Verificamos que las carpetas que tiene los cambios.
- Creamos el gitIgnore, debemos tener encuenta que nuestro proyecto contiene todas las dependecias en su package.json. Asi la persona que vayá a utilizar nuestro proyecto al descargarlo installara las mismas. 
**echo 'node_modules' > .gitignore**

**NOTA:** para agregar un carpeta o archivo al gitignore **echo 'nombre de la carpeta' >> .gitignore** 

## Instalando  nodemon: 
Nos permite que al modificar cualquier arhivo es servidor se recete de forma automatica, esto nos permite que al dirigirnos a la web, estemos trabajando con la ultima verion del server.

- **npm install nodemon--save-dev**

## Boostrap para descargar los html.
- Hacer uso de la pg html to pug.
- Configurar el proyecto.

## Libreria leaflet para trabajar con mapas

### Agregar la libreria al proyecto: 

## Testing con jasmine
Es un framework de desarrollo, guiado por el comportamiento DGC, para testear código javascript.

## Instalando jasmine.
Para agregarlo a nuestro proyecto como dependencia del ambiente de desarrollo, unicamente debemos:

- Primero lo instalamos globlalmente para ejecutarlo directamente desde el terminal. **npm install -g jasmine**.
- Luego lo agregamos como dependencia de desarrollo **npm install --save-dev jasmine**.
- Luego, debemos inicializar el módulo haciendo: **node node_modules/Jasmine/bin/Jasmine init**.
- Agregamos un scrpit "test":"jasmine" en el packeg.json.

**Con ese script para probar los test colocamos en consola (npm test) ó jasmine spec/...path...**

## Request :instalamos request para cuando  testeamos varios req http y controlarlos para facilitar el testin

- **npm install request -save**
- Iniciamos el server **npm start**, si el puerto esta en uso lo cambiamos en la carpeta bin por el 5000.

Para evitar levantar el servidor cada vez que se ejecute una prueba debemos incorporar el servidor como un modulo mas. el modulo que levanta y lo pone en escucha:

- En la carpeta bin/www agregamos  **module.exports = server**.
- Luego la importamos requerimos en nuestro modulo de prueba. 

Como funciona: una ves es solicitado el modulo, se ejecuta todo lo que esta dentro del archivo para poder exportar.

El request es utilizado para hacer uso de las solicitudes http: get,post,deleted,restor, la forma de usarla es la siguiente:

**request.get('/path',function(error,response,body){

});**

## Instalando el ODM mongoose. 

- **npm install mongoose** 

